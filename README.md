Plymouth de Canaima
===================

En este espacio se publicarán los Plymouth que se han desarrollado dentro del Proyecto Canaima, con el fin de compartirlos con todos aquellos usuarios interesados en conocer el funcionamiento de cada uno de ellos. Cabe a destacar que, un Plymouth es aquella animación que aparece cuando la Distribución Canaima GNU/Linux se está iniciando. En la [wiki](https://gitlab.com/ProyectoCanaima/plymouth/-/wikis/home) de este proyecto, podrán encontrar documentación sobre como instalar y configurar un plymouth tomando como base los de Canaima GNU/Linux.

Antes de copiar, distribuir o modificar nuestros códigos, recomendamos leer el archivo [LICENSE.md.](https://gitlab.com/ProyectoCanaima/plymouth/-/blob/master/LICENSE)
